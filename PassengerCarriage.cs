﻿using System;

namespace Homework2
{
    /// <summary>
    /// Класс "Пассажирский вагон"
    /// </summary>
    public class PassengerCarriage : Carriage
    {
        //Максимальное кол-во пассажиров в вагоне
        readonly int _passengerMax;
        //Текущее кол-во пассажиров в вагоне
        int _passengerCount;
        PassengerCarriageType PassengerCarriageType { get; set; }

        public PassengerCarriage(int passengerMax, int passengerCount, PassengerCarriageType passengerCarriageType) : base()
        {
            CarriageType = CarriageType.Passenger;
            PassengerCarriageType = passengerCarriageType;
            _passengerMax = passengerMax;
            _passengerCount = passengerCount;
        }

        /// <summary>
        /// Добавление пассажиров в вагон
        /// </summary>
        /// <param name="passengersCount">Число пассажирова</param>
        public void AddPassengers(int passengerCount)
        {
            if (_passengerCount + passengerCount > _passengerMax)
                throw new OutOfMemoryException("Превышено максимальное число пассажиров");
            _passengerCount += passengerCount;
        }

        /// <summary>
        /// Удаление пассажиров из вагона
        /// </summary>
        /// <param name="passengerCount">Число пассажиров</param>
        public void RemovePassengers(int passengerCount)
        {
            if (_passengerCount - passengerCount < 0)
                throw new OutOfMemoryException("Итоговое число пассажиров не может быть меньше нуля");
            _passengerCount -= passengerCount;
        }

        public override string ToString()
        {
            return base.ToString() + $"{Environment.NewLine} тип пассажирского вагона: {PassengerCarriageType}{Environment.NewLine} количество пассажиров: {_passengerCount}" ;
        }
    }

}
