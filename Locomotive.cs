﻿using System;

namespace Homework2
{
    /// <summary>
    /// Класс локомотив
    /// </summary>
    public class Locomotive
    {
        Guid Id { get;}
        //Тип локомотива
        LocomotiveType LocoType { get; set; }
        //Мощность силовой установки
        int EnginePower { get; set; }

        public Locomotive(LocomotiveType locoType, int enginePower)
        {
            if (enginePower < 0)
                throw new AggregateException("Мощность не может быть отрицательной");
            Id = new Guid();
            LocoType = locoType;
            EnginePower = enginePower;
        }

        public override string ToString()
        {
            return $"Тип локомотива:{Environment.NewLine} {LocoType}{Environment.NewLine} Мощность: {EnginePower}";
        }
    }

}
