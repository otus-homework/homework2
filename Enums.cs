﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    /// <summary>
    /// Тип Вагона
    /// </summary>
    public enum CarriageType
    {        
        //Пассажирский,
        Passenger,
        //Грузовой
        Freight
    }
    
    /// <summary>
    /// Тип пассажирского вагона
    /// </summary>
    public enum PassengerCarriageType
    {
        /// <summary>
        /// ОбщийВагон
        /// </summary>
        SittingCarriage,
        /// <summary>
        /// Плацкарт
        /// </summary>
        CouchetteCarriage,
        /// <summary>
        /// Купе
        /// </summary>
        CompartmentCoach,
        /// <summary>
        ///СВ
        /// </summary>
        CompartmentSleeper
    }

    /// <summary>
    /// Тип грузового вагона
    /// </summary>
    public enum FreightCarriageType
    {
        
        //Открытый,
        Opened,
        //Закрытый
        Closed
    }

    /// <summary>
    /// Тип локомотива
    /// </summary>
    public enum LocomotiveType
    {
        //электровоз
        ElectricLoco,
        //Паровозы
        SteamLoco,
        //Газотурбовоз
        GasTurbineLoco,
        //Тепловоз
        DieselLoco
    }
}
