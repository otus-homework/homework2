﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Homework2
{
    /// <summary>
    /// Класс "Поезд"
    /// </summary>
    public class Train
    {
        //Пункт назначения
        string _destination;
        //Пункт отправления
        string _departure;
        //Дата прибытия
        DateTime _arrivalTime;
        //Дата отправления
        DateTime _departureTime;

        Guid Id { get; }
        //Номер поезда
        public string Number { get; set; }
        //Группа локомотивов
        List<Locomotive> MovableGroup { get; set; }
        //Группа вагонов
        List<Carriage> CarriageGroup { get; set; }

        #region .ctor
        public Train(string number)
        {
            Id = new Guid();
            Number = number.StringWithoutSpaces();
        }

        public Train(string number, List<Locomotive> movableGroup, List<Carriage> carriageGroup) : this(number)
        {
            MovableGroup = movableGroup;
            CarriageGroup = carriageGroup;
        }

        public Train(string dectination, string departure, DateTime destinationTime, DateTime departureTime, string number, List<Locomotive> movableGroup, List<Carriage> carriageGroup) : this(number, movableGroup, carriageGroup)
        {
            _destination = dectination;
            _departure = departure;
            _arrivalTime = destinationTime;
            _departureTime = departureTime;
        }
        #endregion

        //индексатор
        public Carriage this[int index]
        {
            get { return CarriageGroup[index]; }
            set { CarriageGroup[index] = value; }
        }

        #region Methods

        /// <summary>
        /// Задать время и место прибытия
        /// </summary>
        /// <param name="place">Место прибытия</param>
        /// <param name="dateTime">Время прибытия</param>
        public void SetDestination(string place, DateTime dateTime)
        {
            if (String.IsNullOrEmpty(place))
                throw new AggregateException("Не указано место назначения");
            if (_departureTime != DateTime.MinValue && dateTime < _departureTime)
                throw new AggregateException("Время прибытия не может быть меньше времени отправления");
            _destination = place;
            _arrivalTime = dateTime;

        }

        /// <summary>
        /// Задать место прибытия
        /// </summary>
        /// <param name="place">Место прибытия</param>
        public void SetDestination(string place)
        {
            if (String.IsNullOrEmpty(place))
                throw new AggregateException("Не указано место назначения");
            _destination = place;
        }

        /// <summary>
        /// Задать время и место отправления
        /// </summary>
        /// <param name="place">Место отправления</param>
        /// <param name="dateTime">Время отправления</param>
        public void SetDeparture(string place, DateTime dateTime)
        {
            if (String.IsNullOrEmpty(place))
                throw new AggregateException("Не указано место отправления");
            _destination = place;
            _departureTime = dateTime;
        }

        /// <summary>
        /// Задать место отправления
        /// </summary>
        /// <param name="place">Место отправления</param>
        public void SetDeparture(string place)
        {
            if (String.IsNullOrEmpty(place))
                throw new AggregateException("Не указано место отправления");
            _destination = place;
        }

        /// <summary>
        /// Изменение времени в пути
        /// </summary>
        /// <param name="hours">Часы</param>
        /// <param name="minute">Минуты</param>
        public void ChangeTravelTime(int hours, int minute)
        {
            if (_departureTime == DateTime.MinValue)
                throw new AggregateException("Не указано время отправления");
            _arrivalTime = _departureTime + new TimeSpan(hours, minute, 0);
        }

        public string ShowMovableGroup()
        {
            int i = 0;
            string header = "Подвижная группа:";
            if (MovableGroup != null && MovableGroup.Any())
                return header + Environment.NewLine + String.Join(Environment.NewLine, MovableGroup.Select(x => $"№ {++i}, " + x.ToString()));
            else return header + Environment.NewLine + "Нет локомотивов";
        }
        public string ShowCarriageGroup()
        {
            int i = 0;
            string header = "Вагонная группа:";
            if (CarriageGroup != null && CarriageGroup.Any())
                return header + Environment.NewLine + String.Join(Environment.NewLine, CarriageGroup.Select(x => $"№ {++i}, " + x.ToString()));
            else return header + Environment.NewLine + "Нет вагонов";
        }

        public string ShowAllInfo()
        {
            return
            this.ToString() + Environment.NewLine + this.ShowMovableGroup() + Environment.NewLine + this.ShowCarriageGroup();
        }

        // Переопределение метода ToString()
        public override string ToString()
        {
            string nullValue = "Не задано";
            return String.Format(@"Поезд № {1}:{0}  маршрут: {2} - {3}{0}  время отправления: {4}{0} время прибытия: {5}",
                Environment.NewLine, Number, _departure ?? nullValue, _destination ?? nullValue,
                _departureTime != DateTime.MinValue ? _departureTime.ToString() : nullValue,
                _arrivalTime != DateTime.MinValue ? _arrivalTime.ToString() : nullValue);
        }
        #endregion

        #region Operators
        // Добавление вагона в поезд 
        public static Train operator +(Train train, Carriage carriage)
        {
            train.CarriageGroup.Add(carriage);
            return train;
        }

        // Удаление вагона из поезда
        public static Train operator -(Train train, Carriage carriage)
        {
            train.CarriageGroup.Remove(carriage);
            return train;
        }

        //Добавление локомотива в поезд
        public static Train operator +(Train train, Locomotive locomotive)
        {
            train.MovableGroup.Add(locomotive);
            return train;
        }

        //Удаление локомотива из поезда
        public static Train operator -(Train train, Locomotive locomotive)
        {
            train.MovableGroup.Remove(locomotive);
            return train;
        }
        #endregion


    }

}
