﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    class Program
    {
        static void Main(string[] args)
        {
            PassengerCarriage pasCar1 = new PassengerCarriage(30,10, PassengerCarriageType.CompartmentSleeper);
            PassengerCarriage pasCar2 = new PassengerCarriage(30, 15, PassengerCarriageType.CompartmentCoach);
            PassengerCarriage pasCar3 = new PassengerCarriage(55, 30, PassengerCarriageType.CouchetteCarriage);

            FreightCarriage frCar1 = new FreightCarriage(5000, 3000, FreightCarriageType.Opened);
            FreightCarriage frCar2 = new FreightCarriage(10000, 6000, FreightCarriageType.Closed);

            Locomotive loco1 = new Locomotive(LocomotiveType.DieselLoco, 5000);
            Locomotive loco2 = new Locomotive(LocomotiveType.GasTurbineLoco, 3000);

            Train train1 = new Train("A1", new List<Locomotive>(), new List<Carriage>());
            Console.WriteLine(train1.ShowAllInfo());
            train1 += loco1;
            train1 += pasCar1;
            train1 += pasCar2;
            train1 += frCar1;
            Console.WriteLine();
            Console.WriteLine(train1.ShowAllInfo());

            train1 -= loco1;
            train1 += loco2;
            train1 -= pasCar2;
            train1 += frCar2;
            train1 += pasCar3;            
            Console.WriteLine();
            Console.WriteLine(train1.ShowAllInfo());

            pasCar1.AddPassengers(10);
            pasCar3.RemovePassengers(10);
            frCar1.RemoveCargo(500);
            frCar2.AddCargo(500);
            Console.WriteLine();
            Console.WriteLine(train1.ShowAllInfo());
            Console.ReadKey();
            ;
        }
    }
}
