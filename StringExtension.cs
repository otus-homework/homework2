﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    //Класс расширения для string
    public static class StringExtension
    {
        public static string StringWithoutSpaces(this string str)
        {
            return str.Replace(" ", "");
        }
    }
}
