﻿using System;

namespace Homework2
{
    /// <summary>
    /// Класс "Грузовой вагон"
    /// </summary>
    public class FreightCarriage : Carriage
    {
        //Максимальный вес груза в вагоне
        readonly double _weightMax;
        //Текущий вес груза в вагоне
        double _weightCargo;
        public FreightCarriageType FreightCarriageType { get; }

        public FreightCarriage(double weightMax, double weightCargo, FreightCarriageType freightCarriageType) : base()
        {
            CarriageType = CarriageType.Freight;
            _weightMax = weightMax;
            _weightCargo = weightCargo;
            FreightCarriageType = freightCarriageType;
        }

        /// <summary>
        /// Добавление груза в вагон
        /// </summary>
        /// <param name="weight">Вес груза</param>
        public void AddCargo(double weight)
        {
            if (_weightCargo + weight > _weightMax)
                throw new OutOfMemoryException("Превышено максимальной грузоподьемности вагона");
            _weightCargo += weight;
        }

        /// <summary>
        /// Удаление груза из вагона
        /// </summary>
        /// <param name="weight">Вес груза</param>
        public void RemoveCargo(double weight)
        {
            if (_weightCargo - weight < 0)
                throw new OutOfMemoryException("Итоговая грузоподьемность не может быть меньше нуля");
            _weightCargo -= weight;
        }

        public override string ToString()
        {
            return base.ToString() + $"{Environment.NewLine} тип грузового вагона: {FreightCarriageType}{Environment.NewLine} вес груза: {_weightCargo}.";
        }
    }

}
