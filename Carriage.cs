﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2
{
    //Класс "Абстактный вагон"
    public abstract class Carriage
    {
        public Guid Id { get; }
        //Тип вагона
        public CarriageType CarriageType { get; set; }
        protected Carriage()
        {
            Id = new Guid();
        }

        public override string ToString()
        {
            return $"Тип вагона: {CarriageType}";
        }
    }

}
